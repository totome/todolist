package org.todolist.webapp;

import java.io.IOException;
import java.util.Base64;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.todolist.domain.persistence.User;


@WebServlet(urlPatterns = { "/addUser" })
public class AddUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public AddUserServlet() {
        super();
    }

    //TODO: Consider the proper way to test this method automatically. Write tests.
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name = request.getParameter("name");
		String pass = request.getParameter("pass");

		StringBuilder pathBuilder = new StringBuilder();
		pathBuilder.append("signUp.jsp");

		if (name == null || "".equals(name)) {
			pathBuilder.append("?cause=failure")
			.append("&reason=")
			.append(Base64.getEncoder()
					.encodeToString("Username field is required! Please enter username in the field below.".getBytes()));
		} else if (pass==null || "".equals(pass)) {
			pathBuilder.append("?cause=failure")
			.append("&reason=")
			.append(Base64.getEncoder()
					.encodeToString("Password field must not be empty".getBytes()));
		} else {
			String AUTH = "Basic " + java.util.Base64.getEncoder().encodeToString("login_manager:65536".getBytes());
			User user = new User(name, pass);

			String host = "http://localhost";
			String port = "8080";
			String appUri = "todolist-webapp/webapi/users";
			String basicUrl = host+":"+port+"/"+appUri;
			WebTarget target = ClientBuilder.newClient().target(basicUrl);
			Response resp = target.request().header("authorization", AUTH).post(Entity.json(user));

			if(resp == null || resp.getStatus() != 201) {
				String str = resp.readEntity(String.class);
				System.out.println("Entity : "+str);
				pathBuilder.append("?cause=failure")
				.append("&reason=")
				.append(Base64.getEncoder().encodeToString((str).getBytes()));
			}
			else
				pathBuilder.append("?cause=success");
		}

		request.getRequestDispatcher(pathBuilder.toString()).forward(request, response);
	}
}
