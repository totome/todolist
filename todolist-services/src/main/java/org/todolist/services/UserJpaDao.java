package org.todolist.services;

import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;

import org.todolist.domain.persistence.Role;
import org.todolist.domain.persistence.User;

public class UserJpaDao extends JpaDao implements UserDao {
	@Inject
	public UserJpaDao(EntityManagerFactory emf){
		super(emf);
	}

	@Override
	public String create(User user) {
		return completeTransaction(em->{
			User found = em.find(User.class, user.getName());
			if(found == null) {
				em.persist(user);
				Role user_role = em.find(Role.class, Role.REGISTERED_USER);
				user.addRoles(user_role);
				return user.getName();
			} else return null;
		});
	}

	@Override
	public User read(String id) {
		return completeTransaction(em-> {
			return em.find(User.class, id);
		});
	}

}
