TODOLIST
-----------

This project is a programing exercise. When it's completed, it will be a RESTful web service, and a simple client for it. It uses Jersey as a JAX-RS implementation and Java Persistence API for saving data. It is also an excercise in testing the code using common Java frameworks.To manage this project I'm using common technologies like Git, Maven etc.

### It's divided into four modules:  
* __todolist-domain__ - defines abstractions which reproduces items occurring in application model and relations between them; In this case those abstractions represents users and tasks. Every task belong to the user who defined it (user has its own set of activities).  
* __todolist-service__ - contains services which are providing functionalities for the webapp. As for now there are only classes related to the data persistence:
  * DAO Interfaces
  * JPA DAO implementations  
* __todolist-webapp__ - module which produces war file which may be deployed on Tomcat 8.x server. It defines service endpoint to which http requests can be send. Only resource-style paths are allowed (for example "/users/1", "/users/1/tasks/4"). There is also simple website which allows new user to sign up (in the future it will be moved to the different module). To authenticate users app uses Tomcats basic authorization.
* __todolist-client__ - simple client site app which connects to the server. Once you set up an account, you can log in and manage your task list.  

### Requirements: 
* Internet connection
* Java 8 (tested on 1.8.162)
* Maven (tested on 3.3.9)
* Tomcat 8.x server or newer (tested on Tomcat 8.5.31)

### Additional technologies used in the project:
* JPA 2.1
* Hibernate 5.2 (as an implementation of the JPA)
* H2 Database
* Jersey 2.26 (JAX-RS implementation)
* HK2 Dependency Injection Kernel
* JUnit 4.12
* Mockito 2.12
* Bootstrap 4

### Usage:
#### Packaging:
Application is managed by Maven project management tool, so it needs Maven to be installed beforehand (more about Maven can be found at https://maven.apache.org/). Once you have Maven installed go to the root of the project and type :
`mvn clean package`   
After that all sub-projects will be tested, and packaged in the required order. As a result you have got :
* __todolist-webapp - Java web archive__ :  
there should be the war file in the target folder of the todolist-webapp subproject :  
`{project_root}/todolist-webapp/target/todolist.war`  
As for now, this war file includes both: rest application, and simple web page for adding users.   

* __todolist-client - client-app java archive__ :  
in the target folder of this subproject you can find the todolist-client jar file and all its dependencies copied by Maven into the `target/lib/` directory. The jar file must be launched by typing `java -jar {filename}` in the folder containing __both__ jar file and `lib` folder (client target folder contains both of these. If you want to move that program to the other location then you have to move the jar, and the lib).  


* __sub-projects with dependencies__ :
  + __todolist-model - dependencies__ :  
the target folder after packaging phase contains Java archives which are necessary by other modules to work correctly. The most imporant modules there are:
    - __org.todolist.model.persistence__ :
  it contains persistence entities and constructs relationships between them.   
    - __org.todolist.model.http__ :
  includes classes of objects used to transfer data over the http protocol.  
  + __todolist-service__ :
  it contains services and utilities used by other modules to complete some more sophisticated tasks. As for now the most important part of it is module __org.todolist.service.dao__ which contains the data access objects.  

#### launching:  
* __server-site__:  
To launch it: put created in  the previous step war file in the Tomcat 8 server directory. Go to this application website for more instructions.
* __clien-site__:  
__warning: As for now client applicaion will not work if todolist-webapp is not running on the server site, or its URL is unreachable.__  
After the server-site app is running, and the jar file from the clients target folder is launched, you should see login screen. To log in register the user on the web site wich is running on Tomcat server after todolist-webapp has been deployed. Now you can add, edit, and delet your tasks.


#### Additional Information:
##### Database Connection:
   * This app uses the tomcat's default connection pool. It is configured as JNDI resource in: `/todolist-webapp/src/main/webapp/META-INF/context.xml`  
It makes connections with the H2 in-memory named database which means that **data are not persisting after the app is closed**. This behavior is set for testing purpose and will be changed in the future.
