package org.todolist.webapp.filter;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import org.todolist.webapp.utils.CtxUtil;


public class UserAuth implements ContainerRequestFilter {

	@Override
	public void filter(ContainerRequestContext requestContext) throws IOException {
		CtxUtil ctxUtil = getCtxUtil(requestContext);
		String abortCause = null;

		if(ctxUtil.isUserInRole("login_manager")) {
			return;
		} else
		if(!ctxUtil.isUserInRole("user")) {
            abortCause = "authentication failed";
		} else {
			String validUser = ctxUtil.getUserName();
			List<String> userParamList = ctxUtil.getPathParams("userId");

			if (userParamList == null
					|| userParamList.size() != 1
					|| !userParamList.contains(validUser)) {
				abortCause = "not recognized as an requested user";
			}
		}

		if (abortCause != null) ctxUtil.abord(abortCause);
	}

	protected CtxUtil getCtxUtil(ContainerRequestContext requestContext) {
		return new CtxUtil(requestContext);
	}
}