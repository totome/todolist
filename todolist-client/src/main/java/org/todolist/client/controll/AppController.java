package org.todolist.client.controll;

import java.util.List;

import org.todolist.client.model.TaskResource;
import org.todolist.client.model.http.BasicAuth;
import org.todolist.client.model.http.ResourceUnavailable;
import org.todolist.client.view.GUI;
import org.todolist.domain.http.TaskParcel;


public class AppController {
	private TaskResource conn;
	private GuiTasks list;
	private GUI view;
	private Submitter submitter;

	public AppController(TaskResource rm) {
		this.conn = rm;	
	}

	public void setView(GUI view) {
		this.view = view;
		this.list = view.shareTasks();
	}

	public void sync(){
		list.clear();
		List<TaskParcel> tasks = conn.fetchAll();
		if(tasks == null)
			error();
		else {
			list.clear();
			tasks.stream()
			.forEach(t->list.add(t,this));
		}
	}

	public void canceled() {
		submitter = null;
		view.showMain();
	}

	public void delete(TaskParcel t) {
		submitter = Submitter.DELETE;
		submitter.submit(t, conn, this);
	}

	public void addNewTask() {
		submitter = Submitter.ADD;
		view.showTaskEdition(new TaskParcel(null, "", ""));
	}

	public void editTask(TaskParcel t) {
		submitter = Submitter.UPDATE;
		view.showTaskEdition(t);
	}

	public void submit(TaskParcel t) {
		submitter.submit(t, conn, this);
		view.showMain();
	}
	
	public void newLoginDetails(String path, String login, String password) {
		BasicAuth newAuth = new BasicAuth(login, password);
		try {
			conn.setConnectionDetails(path, newAuth);
			view.showMain();
		} catch (ResourceUnavailable e) {
			boolean showInvalidCredentialsMsg = true;
			view.showLogin(showInvalidCredentialsMsg);
		}
	}

	void error() {
		view.showError("Something went wrong");
	}

	private static interface Submitter {
		Submitter ADD = (task, conn, ctrl)->{
			if(conn.create(task)) {
				ctrl.sync();
				return task;
			} else {
				ctrl.error();
				return null;
			}
		};

		Submitter UPDATE = (task, conn, ctrl)->{
			if(conn.update(task)) {
				ctrl.sync();
				return task;
			} else {
				ctrl.error();
				return null;
			}
		};

		Submitter DELETE = (task, conn, ctrl)->{
			if(conn.delete(task)) {
				ctrl.sync();
				return task;
			} else {
				ctrl.error();
				return null;
			}
		};

		TaskParcel submit(TaskParcel task, TaskResource res, AppController ctrl);
	}

}
