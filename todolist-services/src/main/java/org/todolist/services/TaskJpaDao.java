package org.todolist.services;


import java.util.List;
import java.util.stream.Collectors;
import java.util.ArrayList;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.todolist.domain.persistence.Task;
import org.todolist.domain.persistence.User;
import org.todolist.services.exception.DataNotFound;

public class TaskJpaDao extends JpaDao implements TaskDao{
	private String userId;
	
	@Inject
	public TaskJpaDao(EntityManagerFactory emf) {
		super(emf);
	}
	
	@Override
	public void setUserName(String name) {
		this.userId = name;
	}

	@Override
	public Long create(final Task task) {
		return completeTransaction(em->{
			User u = em.find(User.class, userId);
			if(u == null)throw new DataNotFound("No such user");
			u.getTasks().add(task);
			return task.getId();
			});
	}

	@Override
	public Task read(long id) {
		return completeTransaction(em->findOrThrow(em, id));
	}

	@Override
	public Task update(Task newVersion) {
		return completeTransaction(em->{
			Task old = findOrThrow(em, newVersion.getId());
			old.setDescription(newVersion.getDescription());
			old.setTitle(newVersion.getTitle());
			return em.merge(old);
		});
	}

	@Override
	public Task delete(long id) {
		return completeTransaction(em->{
				Task toRm = findOrThrow(em, id);
				em.remove(toRm);
				return toRm;
		});
	}

	//TODO : consider testing scenarios for this method
	@Override
	public List<Task> getAll() {
		return completeTransaction(em->{
			User u = em.find(User.class, userId);
			return u.getTasks().stream()
					.collect(Collectors.toCollection(ArrayList::new));
		});
	}
	
	private Task findOrThrow(EntityManager em, long id) {
		Task result = em.find(Task.class, id);
		if(result == null || result.getUserName() == null || !result.getUserName().equals(userId))
			throw new DataNotFound("task with id = "+id+" is not preasent in persistence unit");
		return result;
	}
}
