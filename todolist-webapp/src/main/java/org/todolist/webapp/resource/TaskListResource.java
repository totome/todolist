package org.todolist.webapp.resource;

import java.util.List;
import java.util.function.Supplier;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.todolist.domain.persistence.Task;
import org.todolist.services.TaskDao;
import org.todolist.services.exception.DataNotFound;
import org.todolist.services.exception.OperationFailed;


public class TaskListResource {
	@Inject private TaskDao tasks;

	@GET
	@Produces(value= {MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
	public Response getAll() {
		List<Task> all = tasks.getAll();
		if(all == null || all.isEmpty())
			return Response.status(Status.NO_CONTENT).build();
		else {
			GenericEntity<List<Task>> res = new GenericEntity<List<Task>>(all) {};
			return Response.ok().entity(res).build();
		}
	}

	@GET
	@Produces(value = {MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
	@Path("/{taskId}")
	public Response getTask(@PathParam("taskId") long id) {
		Task result = translateErrors(()->tasks.read(id));
		return Response.ok()
				.entity(result)
				.build();
	}
	
	@POST
	@Consumes(value = {MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
	public Response writeTask(Task task) {
		task.setId(null);
		tasks.create(task);
		return Response
				.status(Status.CREATED)
				.build();
	}
	
	@DELETE
	@Path("{id}")
	@Produces(value = MediaType.APPLICATION_JSON)
	public Response deleteTask(@PathParam("id") long id) {
		Task deleted = tasks.delete(id);
		return Response
				.status(Status.OK)
				.entity(deleted)
				.build();
	}
	
	@PUT
	@Path("{id}")
	@Consumes(value = {MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
	@Produces(value = {MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
	public Response updateTask(Task given, 
					@PathParam("id") long id) {
		given.setId(id);
		Task updated = tasks.update(given);

		return Response
				.status(Status.OK)
				.entity(updated)
				.build();
	}
	
	private <R> R translateErrors(Supplier<? extends R> action){
		try {
			return action.get();
		}catch(DataNotFound dnf) {
			throw new NotFoundException();
		}catch(OperationFailed of) {
			throw new InternalServerErrorException();
		}
	}
}
