package org.todolist.webapp;

import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.process.internal.RequestScoped;
import org.todolist.domain.persistence.Role;
import org.todolist.domain.persistence.Task;
import org.todolist.domain.persistence.User;
import org.todolist.services.*;

public class ServiceInjectionBinder extends AbstractBinder {
	@Override
	protected void configure() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("org.todolist.services.jpa");

		bind(emf).to(EntityManagerFactory.class);
		bind(TaskJpaDao.class).to(TaskDao.class).in(RequestScoped.class);
		bind(UserJpaDao.class).to(UserDao.class).in(Singleton.class);
		
		addDbDefaults(emf.createEntityManager());
	}

	/**
	 * TODO : this method should be removed if the functionality of adding new users is implemented.
	 * Until it's completed, this method allows the client to act as a default user.
	 * As for now It also adds to the data base roles required by the app : user and login_manager.
	 */
	private void addDbDefaults(EntityManager em) {
		EntityTransaction tr = em.getTransaction();
		try {
			tr.begin();
			User user = new User("default", "password");
			Role role = new Role("user");
			em.persist(user);
			em.persist(role);
			user.addRoles(role);
			Task task = new Task(null, "TODO",
					"Remove addDefaultUser method \nfrom the ServiceInjectionBinder");
			user.getTasks().add(task);

			Role manager_role = new Role("login_manager");
			em.persist(manager_role);
			User manager = new User("login_manager", "65536");
			em.persist(manager);
			manager.addRoles(manager_role);

			tr.commit();
		} catch (RuntimeException ex) {
			tr.rollback();
			ex.printStackTrace();
		} finally {
			em.close();
		}
	}
}
