package org.todolist.client.model.http;

import java.util.function.Function;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;

public interface RequestBuilder {
	RequestBuilder setAuth(BasicAuth auth);
	RequestBuilder appendPath(String path);
	RequestBuilder setAction(Function<Invocation.Builder, Response> action);
	RequestBuilder setDecoration(Function<Req, Req> action);
	Req getFrom(Client client);
}