<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>TODOLIST</title>
	<%@ include file="/WEB-INF/fragments/bootstrapInit.jspf"%>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body style="background-color:lightgray;">
	<%@ include file="/WEB-INF/fragments/navbar.jspf"%>
	<%StringBuffer url = request.getRequestURL();%>
	<%String ctxUrl = request.getContextPath();%>
	<%String reqUri = request.getRequestURI();%>
	<%int urlLength = url.length() - reqUri.length() + ctxUrl.length();%>
	<%String apiUrl = url.substring(0, urlLength)+"/webapi"; %>

	<div class="jumbotron jumbotron-fluid bg-secondary text-white">
		<div class="container">
			<p>To use the todolist app:</p>
			<ul>
				<li>activate the new user by clicking on sign up link above and submit the user name and password</li>
				<li>after registration, you can reach the api endpoints using the basic authorization with that username and password</li>
				<li>compile the todolist-client subproject by running mvn package in the todolist-client directory</li>
				<li>launch the created in previous step jar file from the todolist-client/target folder</li>
				<li>set the user, password and the path to the API on the client-app welcome screen</li>
			</ul>
			<div class="alert alert-warning">
			<strong>the path to the API is : <%=apiUrl%></strong>
			</div>
		</div>
	</div>
</body>
</html>