package org.todolist.webapp.filter;

import java.io.IOException;
import java.util.Collections;

import javax.ws.rs.container.ContainerRequestContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.todolist.webapp.utils.CtxUtil;

public class UserAuthTest{
	static final String CORRECT_USER = "0xBEEF";
	static final String FAULTY_USER = "0xBAD";
	@Spy
	ContainerRequestContext ctx;
	@Mock
	CtxUtil util;
	@Spy
	UserAuth sut;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		Mockito.doReturn(util).when(sut).getCtxUtil(Mockito.any());
	}

	@Test
	public void whenUserIsNotAuthenticatedByTheContainerThenHisRequestIsAborted() throws IOException{
		when(util.isUserInRole(Mockito.anyString())).thenReturn(false);
		when(util.getUserName()).thenReturn(null);
		when(util.getPathParams("userId")).thenReturn(Collections.singletonList(FAULTY_USER));

		sut.filter(ctx);

		verify(util).abord(Mockito.anyString());
	}

	@Test
	public void whenUserInThePathIsDiffrentThanTheAuthenticatedUserThenHisRequestIsAborted() throws IOException {
		when(util.isUserInRole(Mockito.anyString())).thenReturn(false);
		when(util.isUserInRole("user")).thenReturn(true);
		when(util.getUserName()).thenReturn(CORRECT_USER);
		when(util.getPathParams("userId")).thenReturn(Collections.singletonList(FAULTY_USER));

		sut.filter(ctx);

		verify(util).abord(Mockito.anyString());
	}

	@Test
	public void allUsersRequestsShouldHaveTheUserIdInThePath() throws IOException {
		when(util.isUserInRole(Mockito.anyString())).thenReturn(false);
		when(util.isUserInRole("user")).thenReturn(true);
		when(util.getUserName()).thenReturn(CORRECT_USER);
		when(util.getPathParams("userId")).thenReturn(null);

		sut.filter(ctx);

		verify(util).abord(Mockito.anyString());
	}

	@Test
	public void whenTherIsCorrectUserIdInThePathThenRequestIsNotAborted() throws IOException {
		when(util.isUserInRole(Mockito.anyString())).thenReturn(false);
		when(util.isUserInRole("user")).thenReturn(true);
		when(util.getUserName()).thenReturn(CORRECT_USER);
		when(util.getPathParams("userId")).thenReturn(Collections.singletonList(CORRECT_USER));

		sut.filter(ctx);

		verify(util, Mockito.never()).abord(Mockito.anyString());
	}

	@Test
	public void validatedLoginManagerHasAccessWithoutAnyRestrictions() throws IOException {
		when(util.isUserInRole(Mockito.anyString())).thenReturn(false);
		when(util.isUserInRole("login_manager")).thenReturn(true);

		sut.filter(ctx);

		verify(util, Mockito.never()).abord(Mockito.anyString());
	}

}
