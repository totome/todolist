package org.todolist.client.model.http;

import static org.junit.Assert.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import static org.mockito.Mockito.*;
import java.util.function.Function;

public class ReqAndHttpReqBuilderTest {
	private HttpReqBuilder sut;
	@Mock
	private Client client;
	@Mock
	private WebTarget target;
	@Mock
	private Invocation.Builder invBuilder;
	@Mock
	private Function<Invocation.Builder, Response> action;
	@Mock
	private Response resp;
	@Captor
	private ArgumentCaptor<String> str0;
	@Captor
	private ArgumentCaptor<String> str1;

	private static final String INIT_PATH = "abcdefgh";
	private static final BasicAuth auth = new BasicAuth("12345", "asdfghj");

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		sut = new HttpReqBuilder(INIT_PATH);

		when(client.target(anyString())).thenReturn(target);
		when(target.request()).thenReturn(invBuilder);
		when(action.apply(any())).thenReturn(resp);
	}

	@Test
	public void builderThrowsExceptionDuringConstructionIfNoActionIsSet() {
		try {
			sut.getFrom(client);
			fail("Exception is expected to be thrown");
		} catch (IllegalStateException ex) {}
	}

	@Test
	public void builderSetsPathAndAction() {
		Req req = sut.setAction(action).getFrom(client);

		verify(client, times(1)).target(str0.capture());
		final String path = str0.getValue();
		verify(target, times(1)).request();

		assertEquals(INIT_PATH, path);

		req.make();
		verify(action).apply(invBuilder);
	}

	@Test
	public void builderSetsPathCollectedDurringTheBuildProcessAndCorrectAuthenticationHeader() {
		final String first= "/jkl";
		final String second = "bnm/";
		final String third = "aaa";

		sut
		.appendPath(first)
		.setAction(action)
		.appendPath(second);

		sut
		.setAuth(auth)
		.appendPath(third);

		sut.getFrom(client);

		verify(client, times(1)).target(str0.capture());
		final String path = str0.getValue();
		String expected = INIT_PATH+first+"/"+second+third;
		assertEquals(expected, path);

		verify(target, times(1)).request();
		verify(invBuilder).header(str0.capture(), str1.capture());
		final String authType = str0.getValue();
		final String authStr = str1.getValue();
		assertEquals(auth.AUTH, authType);
		assertEquals(auth.toString(), authStr);
	}

	@Test
	public void ReqReturnsResponseWhenItIsRequestedForIt() {
		Req sut = new HttpReq(invBuilder, action);

		doReturn(resp).when(action).apply(invBuilder);
		sut.make();
		verify(action).apply(invBuilder);

		assertSame(resp, sut.get(Response.class));
	}

	@Test
	public void ReqGetsEntityFromResponseWhenItIsRequestedForIt() {
		Req sut = new HttpReq(invBuilder, action);
		final Object expected = new Object();

		doReturn(resp).when(action).apply(invBuilder);
		doReturn(expected).when(resp).readEntity(Object.class);
		sut.make();
		verify(action).apply(invBuilder);

		assertSame(expected, sut.get(Object.class));
	}
}
