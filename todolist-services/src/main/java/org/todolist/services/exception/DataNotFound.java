package org.todolist.services.exception;

/*
 * this exception should be thrown by service when it can not return wanted data 
 */
public class DataNotFound extends OperationFailed{
	/**
	 * 
	 */
	private static final long serialVersionUID = 254795406352022100L;

	public DataNotFound(String message) {
		super(message);
	}
}
