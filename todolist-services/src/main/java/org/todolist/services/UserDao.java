package org.todolist.services;

import org.todolist.domain.persistence.User;

public interface UserDao {
	String create(User user);
	User read(String id);
}
