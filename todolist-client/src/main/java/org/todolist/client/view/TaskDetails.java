package org.todolist.client.view;

import java.io.IOException;

import org.todolist.client.controll.AppController;
import org.todolist.domain.http.TaskParcel;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

public class TaskDetails extends HBox{
	private final TaskParcel task;
	private final AppController ctrl;

	@FXML private TextField title;
	@FXML private TextArea desc;

	public TaskDetails(final TaskParcel task, final AppController ctrl) {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TaskDetails.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
          throw new RuntimeException(exception);
        }

		title.setText(task.getTitle());
		desc.setText(task.getDescription());


        this.ctrl = ctrl;
        this.task = task;
	}

	@FXML
	private void cancel() {
		ctrl.canceled();
	}

	@FXML
	private void submit() {
		ctrl.submit(new TaskParcel(task.getId(), title.getText(), desc.getText()));
	}
}
