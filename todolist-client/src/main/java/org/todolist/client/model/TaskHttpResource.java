package org.todolist.client.model;

import java.util.Collections;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;

import org.todolist.client.model.http.BasicAuth;
import org.todolist.client.model.http.HttpReqBuilder;
import org.todolist.client.model.http.Req;
import org.todolist.client.model.http.RequestBuilder;
import org.todolist.client.model.http.ResourceUnavailable;
import org.todolist.domain.http.Link;
import org.todolist.domain.http.TaskParcel;
import org.todolist.domain.http.UserParcel;

public class TaskHttpResource implements TaskResource{
	private Client client;
	private BasicAuth auth;
	private String basePath;

	public TaskHttpResource() {}

	@Override
	public void setConnectionDetails(String path, BasicAuth auth) throws ResourceUnavailable{
		if(client != null) client.close();
		client = ClientBuilder.newClient();

		this.basePath = path;

		Req req = new HttpReqBuilder(path)
					.setAuth(auth)
					.appendPath("users")
					.appendPath(auth.getUserName())
					.setAction(b->b.get())
					.getFrom(client);
		UserParcel user = null;
		try {
			req.make();
			user = req.get(UserParcel.class);
		} catch (Exception ex) {
				client.close();
				client = null;
				ResourceUnavailable newEx =  new ResourceUnavailable(
						"The users account is unaccessible for the given credentials.");
				newEx.initCause(ex);
				throw newEx;
		}
		Link tasksUrl = user.getLinks()
			.stream().filter(l->"tasks".equals(l.getRel()))
			.findFirst().orElseThrow(()->new ResourceUnavailable(
					"The Task List asociated with a given users account is unreachable."));
		this.basePath = tasksUrl.getLink();
		this.auth = auth;
	}

	@Override
	public List<TaskParcel> fetchAll() {
		Req req = getBasicReqSetup().setAction(r->r.get()).getFrom(client);
		Response resp = req.make().get(Response.class);
		if (resp == null)
			return null;
		else if (req.getStatus() == 204)
			return Collections.emptyList();
		else
			return resp.readEntity(new GenericType<List<TaskParcel>>() {});
	}

	@Override
	public boolean create(TaskParcel task) {
		Req req = getBasicReqSetup().setAction(r->r.post(Entity.<TaskParcel>json(task)))
					.getFrom(client);
		if(req.make().getStatus() == 201)
			return true;
		else
			return false;
	}

	@Override
	public boolean update(TaskParcel task) {
		Req req = getBasicReqSetup().appendPath(""+task.getId())
				.setAction(r->r.put(Entity.<TaskParcel>json(task)))
				.getFrom(client);
		if(req.make().getStatus() == 200)
			return true;
		else
			return false;
	}

	@Override
	public boolean delete(TaskParcel task) {
		Req req = getBasicReqSetup().appendPath(""+task.getId())
					.setAction(r->r.delete())
					.getFrom(client);
		if (req.make().getStatus() == 200)
			return true;
		else
			return false;
	}

	private RequestBuilder getBasicReqSetup() {
		return new HttpReqBuilder(basePath)
				.setAuth(auth)
				.setDecoration(req->new ReqWithNoEx(req));
	}
}
