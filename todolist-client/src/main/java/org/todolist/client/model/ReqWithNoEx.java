package org.todolist.client.model;

import org.todolist.client.model.http.Req;

class ReqWithNoEx implements Req {
	private final Req req;

	ReqWithNoEx(Req req){
		this.req = req;
	}

	@Override
	public Req make() {
		try {
			req.make();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return this;
	}

	@Override
	public <T> T get(Class<T> clazz) {
		T result = null;
		try {
			result = req.get(clazz);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return result;
	}

	@Override
	public int getStatus() {
		return req.getStatus();
	}
}