package org.todolist.domain.persistence;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 * as for now this class exists only for the sake of 
 * compatibility with tomcat's authentication mechanism
 */
@Entity
@Table(name = "users")
@XmlRootElement
public class User {
	private String name;
	private String password;
	private Set<Role> roles; 
	private Set<Task> tasks;

	public User(){
		this.roles = new HashSet<Role>();
		this.tasks = new HashSet<Task>();
	}
	
	public User(String name, String password) {
		this();
		this.name = name;
		this.password = password;
	}
	
	@XmlTransient
	@OneToMany(cascade = CascadeType.ALL, 
			   orphanRemoval = true)
	@JoinColumn(name="user_name")
	public Set<Task> getTasks() {
		return tasks;
	}

	protected void setTasks(Set<Task> tasks) {
		this.tasks = tasks;
	}
	
	@NotNull
	@ManyToMany(mappedBy="users", fetch=FetchType.EAGER)
	@XmlTransient
	protected Set<Role> getRoles() {
		return roles;
	}

	protected void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Id
	public String getName() {
		return name;
	}
	
	protected void setName(String name) {
		this.name = name;
	}
	
	@NotNull
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public void addRoles(Role...newRoles) {
		for (Role r : newRoles) {
			getRoles().add(r);
			r.getUsers().add(this);
		}
	}
	
	@Transient
	public boolean isInRole(String roleName) {
		return getRoles().stream()
				.anyMatch(r->r.getName().equals(roleName));
	}

	@Override
	public int hashCode() {
		return ((name == null) ? 0 : name.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof User))
			return false;
		User other = (User) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
