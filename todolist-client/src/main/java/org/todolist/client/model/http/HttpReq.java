package org.todolist.client.model.http;

import java.util.function.Function;

import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;

public class HttpReq implements Req {
	private final Invocation.Builder req;
	private Response resp;
	private Function<Invocation.Builder, Response> action;

	HttpReq(Invocation.Builder builder, Function<Invocation.Builder, Response> action){
		this.action = action;
		this.req = builder;
	}

	@Override
	public Req make() {
		resp = action.apply(req);
		return this;
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T get(Class<T> clazz) {
		if (clazz == Response.class)
			return (T) resp;
		else
			return resp.readEntity(clazz);
	}

	@Override
	public int getStatus() {
		return resp.getStatus();
	}
}