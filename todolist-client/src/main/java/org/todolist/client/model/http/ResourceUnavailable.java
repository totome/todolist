package org.todolist.client.model.http;

public class ResourceUnavailable extends Exception {
	private static final long serialVersionUID = 5833197474103266740L;

	public ResourceUnavailable(String cause) {
		super(cause);
	}
}
