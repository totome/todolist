package org.todolist.webapp.resource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.LinkedList;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.model.Resource;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.todolist.domain.persistence.Task;
import org.todolist.services.TaskDao;
import org.todolist.services.exception.DataNotFound;
import org.todolist.services.exception.OperationFailed;

public class TaskListResourceTest extends JerseyTest{
	static final long DUMMY_ID = 0xCAFE_C00L;
	static final Task DUMMY_TASK = new Task(DUMMY_ID,
						"dummy title of "+DUMMY_ID,
						"dummy description of "+DUMMY_ID);
	static final long RESULT_ID = 0xC0FFEE;
	static final Task RESULT = new Task(RESULT_ID,
						"dummy title of "+RESULT_ID,
						"dummy description of "+RESULT_ID);
	@Mock
	private TaskDao mockedDao;
	@Captor
	private ArgumentCaptor<Task> arg0;
	
    @Override
    protected Application configure() {
    	MockitoAnnotations.initMocks(this);
    	AbstractBinder testBinder = new AbstractBinder() {
    		@Override
    		protected void configure() {
    			bind(mockedDao).to(TaskDao.class);
    		}
    	};

        ResourceConfig testConfig = new ResourceConfig();
        testConfig.registerResources(Resource.builder(TaskListResource.class).path("/tasks").build());
        testConfig.register(testBinder);
        return testConfig;
    }
    
	private Response makeGetTaskRequest() {
		final Response result = target("/tasks").path(""+DUMMY_ID).request().get();
		Mockito.verify(mockedDao, Mockito.atLeastOnce()).read(DUMMY_ID);
		return result;
	}
	
	private Response interruptGettingRequestWithError(Class<? extends Throwable> cl) {
		Mockito.when(mockedDao.read(Mockito.anyLong())).thenThrow(cl);
		final Response result = makeGetTaskRequest();
		assertFalse(result.hasEntity());
		return result;
	}
	
	//GET single instance
	@Test
	public void whenThereIsNoTasksOnTheListThenTasksResourceShouldRepliesWithNoContent() {
		final Response result = target("/tasks").request().get();
		assertFalse(result.hasEntity());
		assertEquals(Status.NO_CONTENT.getStatusCode(), result.getStatus());
	}
	
	@Test
	public void whenAskingForTaskFailsResourceRepliesWithError() {
		assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), 
				interruptGettingRequestWithError(OperationFailed.class).getStatus());
	}
	@Test
	public void whenClientAsksForTaskWhichDoesNotExistsThenResourceRepliesWithError() {
		assertEquals(Status.NOT_FOUND.getStatusCode(), 
				interruptGettingRequestWithError(DataNotFound.class).getStatus());
	}
	@Test
	public void whenClientAsksForExistingTaskThenResourceRepliesWithMessageContainingThisTask() {
		Mockito.when(mockedDao.read(Mockito.anyLong())).thenReturn(DUMMY_TASK);
		final Response result = makeGetTaskRequest();
		assertEquals(Status.OK.getStatusCode(), result.getStatus());
		assertTrue(result.hasEntity());
		Task resTask = result.readEntity(Task.class);

		assertEquals(DUMMY_TASK, resTask);
	}
	
	//POST single instance
	//id should not be set because it should be added by JPA implementation;
	//TODO : consider whether above behavior makes sense;
	@Test
	public void whenClientSendsTaskThenThisTaskIsAddedToTheListButWithoutId() {
		Entity<Task> request = Entity.entity(DUMMY_TASK, MediaType.APPLICATION_JSON);
		target("tasks").request().post(request);
		Mockito.verify(mockedDao, Mockito.times(1)).create(arg0.capture());
		Task givenArg = arg0.getValue();
		assertEquals(DUMMY_TASK.getTitle(), givenArg.getTitle());
		assertEquals(DUMMY_TASK.getDescription(), givenArg.getDescription());
		assertEquals(null, givenArg.getId());
	}
	
	//DELETE single instance
	@Test
	public void whenResourceRecivesDeleteRequestWithIdThenItDeletesCorrespondingTask() {
		Response result = target("tasks").path(""+DUMMY_ID).request().delete();
		Mockito.verify(mockedDao).delete(DUMMY_ID);
		assertEquals(Status.OK.getStatusCode(), result.getStatus());		
	}
	
	@Test
	public void thereIsNoDeleteOperationPresentInService() {
		Response resp = target("tasks").request().delete();
		assertEquals(Status.METHOD_NOT_ALLOWED.getStatusCode(), resp.getStatus());
	}
	
	//UPDATE single instance
	@Test
	public void PUT_returnsResultOfUpdateMethod() {
		Entity<Task> entity = Entity.entity(DUMMY_TASK, MediaType.APPLICATION_JSON);
		Mockito.when(mockedDao.update(Mockito.any(Task.class))).thenReturn(RESULT);
		
		Long ID_FROM_PATH = 0x600D_1DL;
		Response resp = target("tasks").path(""+ID_FROM_PATH).request().put(entity);
		assertEquals(Status.OK.getStatusCode(), resp.getStatus());

		Mockito.verify(mockedDao).update(arg0.capture());
		assertEquals(ID_FROM_PATH, arg0.getValue().getId());

		Task resTask = resp.readEntity(Task.class);
		assertEquals(RESULT, resTask);
	}
	
	//GET all
	@Test
	public void whenThereIsSingleTaskOnTheListThenListContainingThatTaskIsReturned() {
		List<Task> res = new LinkedList<Task>();
		res.add(RESULT);
		Mockito.when(mockedDao.getAll()).thenReturn(res);
		final Response result = target("/tasks").request().get();

		Mockito.verify(mockedDao).getAll();
		assertEquals(Status.OK.getStatusCode(), result.getStatus());

		List<Task> resultList = result.readEntity(new GenericType<List<Task>>(){});
		assertEquals(res, resultList);
	}

}
