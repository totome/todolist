package org.todolist.client.controll;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import static org.mockito.Mockito.*;

import org.todolist.client.model.TaskResource;
import org.todolist.client.model.TaskHttpResource;
import org.todolist.client.model.http.BasicAuth;
import org.todolist.client.model.http.ResourceUnavailable;
import org.todolist.client.view.GUI;
import org.todolist.domain.http.TaskParcel;

/**
 * This test checks correctness of managing data flow 
 * by controller. To do that test performs some operation
 * which are available from UI and then checks data at both sites:
 * GUI site list represented by uiList and 
 * Server site obtained from prepared above fake ConnectionMaker
 */
public class AppControllerTest {
	private TaskParcel[] initRemoteTasks;
	private TaskResource res;
	private AppController sut;
	private FakeGuiTasks guiTasks;
	private GUI view;

	private static String LOGIN = "4*iftt(";
	private static String PASSWORD = "*(&*&*aaa";
	private static String URL = "443724zxc";
	private static BasicAuth AUTH = new BasicAuth(LOGIN, PASSWORD);

	private TaskParcel[] getDummyResources() {
		TaskParcel[] tasks = {
				new TaskParcel(1L, "123", "456"),
				new TaskParcel(2L, "444", "222"),
				new TaskParcel(0xC00L, "111", "000"),
				new TaskParcel(123L, "fake", "fake"),
				new TaskParcel(0xC00FFEEL, "dummy", "dummy task")};
		return tasks;
	}

	@Before
	public void setUp() throws Exception {
		initRemoteTasks = getDummyResources();
		res = new FakeResource(initRemoteTasks);
		guiTasks = new FakeGuiTasks(new ArrayList<TaskParcel>());
		view = mock(GUI.class);
		sut = new AppController(res);

		when(view.shareTasks()).thenReturn(guiTasks);
		sut.setView(view);
	}

	@Test
	public void testSync() {
		assertTrue(guiTasks.list.isEmpty());
		sut.sync();
		assertThatDataOnBothSitesAreEqualTo(Arrays.asList(initRemoteTasks));
	}

	@Test
	public void testDelete() {
		sut.sync();
		
		assertThatDataOnBothSitesAreEqualTo(Arrays.asList(initRemoteTasks));

		sut.delete(initRemoteTasks[0]);
		sut.delete(initRemoteTasks[1]);
		sut.delete(initRemoteTasks[2]);
		sut.delete(initRemoteTasks[3]);
		
		assertThatDataOnBothSitesAreEqualTo(Collections.singletonList(initRemoteTasks[4]));
	}

	@Test
	public void testAdd() {
		sut.sync();
		assertThatDataOnBothSitesAreEqualTo(Arrays.asList(initRemoteTasks));

		sut.addNewTask();
		sut.submit(new TaskParcel(44L, "new", "task"));

		List<TaskParcel> result = new ArrayList<TaskParcel>(Arrays.asList(initRemoteTasks));
		result.add(new TaskParcel(44L, "new", "task"));
		assertThatDataOnBothSitesAreEqualTo(result);
	}

	@Test
	public void testUpdate() {
		sut.sync();

		assertThatDataOnBothSitesAreEqualTo(Arrays.asList(initRemoteTasks));

		TaskParcel update = initRemoteTasks[0];
		update.setTitle("d90f440");
		update.setDescription("aaa342093");

		sut.editTask(update);
		sut.submit(update);

		assertThatDataOnBothSitesAreEqualTo(Arrays.asList(initRemoteTasks));
	}

	@Test
	public void whenOperationsAreCancelledSutDoesNothing() {
		sut.sync();

		assertThatDataOnBothSitesAreEqualTo(Arrays.asList(initRemoteTasks));

		TaskParcel update = new TaskParcel(initRemoteTasks[0].getId(), "87fs89", "abb");

		sut.editTask(update);
		sut.canceled();

		sut.addNewTask();
		sut.canceled();

		assertThatDataOnBothSitesAreEqualTo(Arrays.asList(initRemoteTasks));
	}

	@Test
	public void moreExpandedFlow() {
		sut.sync();
		assertThatDataOnBothSitesAreEqualTo(Arrays.asList(initRemoteTasks));
		final TaskParcel update = initRemoteTasks[0];
		update.setTitle("d90f440");
		update.setDescription("aaa342093");
		final List<TaskParcel> result = new ArrayList<>(Arrays.asList(initRemoteTasks));
		final TaskParcel added0 = new TaskParcel(null, "87fs89", "abb");
		result.add(added0);
		final TaskParcel added1 = new TaskParcel(null, "87fs89", "abb");
		result.add(added1);

		sut.editTask(update);
		sut.submit(update);

		sut.addNewTask();
		sut.submit(added0);

		sut.addNewTask();
		sut.canceled();

		sut.addNewTask();
		sut.submit(added0);

		assertThatDataOnBothSitesAreEqualTo(result);
	}
	
	private ArgumentCaptor<BasicAuth> authArg = ArgumentCaptor.forClass(BasicAuth.class); 
	
	@Test
	public void whenCredentialsAreValidThenSutRequestsForMainScreen() throws ResourceUnavailable {
		GUI gui = Mockito.mock(GUI.class);
		res = Mockito.mock(TaskHttpResource.class);
		sut = new AppController(res);
		sut.setView(gui);
		
		doNothing().when(res).setConnectionDetails(anyString(), any());
		
		sut.newLoginDetails(URL, LOGIN, PASSWORD);
		setConnectionDetailsMethodIsInwokedWithGivenArgs();
		verify(gui).showMain();
	}
	
	@Test
	public void whenCredentialsAreNotValidThenSutRequestsForLoginScreenWithWarning() throws ResourceUnavailable {
		GUI gui = Mockito.mock(GUI.class);
		res = Mockito.mock(TaskHttpResource.class);
		sut = new AppController(res);
		sut.setView(gui);
		
		doThrow(ResourceUnavailable.class).when(res).setConnectionDetails(anyString(), any());
		
		sut.newLoginDetails(URL, LOGIN, PASSWORD);
		setConnectionDetailsMethodIsInwokedWithGivenArgs();
		verify(gui).showLogin(true);
		verify(gui, times(0)).showMain();
	}
	
	private void setConnectionDetailsMethodIsInwokedWithGivenArgs() {
		try {
			verify(res).setConnectionDetails(eq(URL), authArg.capture());
		} catch (ResourceUnavailable e) {
			e.printStackTrace();
		}
		BasicAuth givenAuth = authArg.getValue();
		assertEquals(AUTH, givenAuth);
	}

	private void assertThatDataOnBothSitesAreEqualTo(List<TaskParcel> given) {
		List<TaskParcel> fromRes = res.fetchAll();
		List<TaskParcel> fromGui = guiTasks.list;
		assertEquals(given.size(), fromRes.size());
		assertEquals(given.size(), fromGui.size());

		fromRes.sort(taskCmp);
		fromGui.sort(taskCmp);
		Iterator<TaskParcel> resIt = fromRes.iterator();
		Iterator<TaskParcel> guiIt = fromGui.iterator();

		given.stream().sorted(taskCmp).forEachOrdered(t->{
			assertTaskParcelsAreEqual(t, resIt.next());
			assertTaskParcelsAreEqual(t, guiIt.next());
		});
	}

	private void assertTaskParcelsAreEqual(TaskParcel a, TaskParcel b) {
		assertEquals(a.getId(), b.getId());
		assertEquals(a.getTitle(), b.getTitle());
		assertEquals(a.getDescription(), b.getDescription());
	}

	private final Comparator<TaskParcel> taskCmp = new Comparator<TaskParcel>() {
		public int compare(TaskParcel a, TaskParcel b) {
			int cmp = nullCmp(a.getId(), b.getId());
			if (cmp != 0) return cmp;
			cmp = nullCmp(a.getTitle(), b.getTitle());
			if (cmp != 0) return cmp;
			return nullCmp(a.getDescription(), b.getDescription());
		};

		private <T extends Comparable<T>> int nullCmp(T a, T b) {
			if (a != null) {
				if (b != null)
					return b.compareTo(b);
				else //a is bigger than null
					return 1;
			} else {
				if (b != null)//b is bigger than null
					return -1;
				else//are same
					return 0;
			}
		}
	};
}

class FakeGuiTasks extends GuiTasks {
	public final ArrayList<TaskParcel> list;

	public FakeGuiTasks(ArrayList<TaskParcel> list) {
		super(null);
		this.list = list.stream()
			.map(t->new TaskParcel(t.getId(), t.getTitle(), t.getDescription()))
			.collect(Collectors.toCollection(ArrayList::new));
	}

	@Override
	public boolean add(TaskParcel task, AppController ctrl) {
		TaskParcel toAdd =
				new TaskParcel(task.getId(), task.getTitle(), task.getDescription());
		return list.add(task);
	}

	@Override
	public void clear() {
		list.clear();
	}	
}

class FakeResource implements TaskResource {
	private static final String UNSUPPORTED_OPERATION_ERROR =
			"Invocation of this method of FakeResource is not suported."
			+" To use this method in a test use a mock of that class.";
	private ArrayList<TaskParcel> allTasks;

	public FakeResource(TaskParcel... tasks) {
		allTasks = Arrays.asList(tasks).stream()
				.map(t->new TaskParcel(t.getId(),t.getTitle(),t.getDescription()))
				.collect(Collectors.toCollection(ArrayList::new));
	}

	@Override
	public List<TaskParcel> fetchAll() {
		return allTasks;
	}

	@Override
	public boolean create(TaskParcel task) {
		return allTasks.add(task);
	}

	@Override
	public boolean delete(TaskParcel task) {
		return allTasks.remove(task);
	}

	@Override
	public void setConnectionDetails(String path, BasicAuth auth) {
		throw new UnsupportedOperationException(UNSUPPORTED_OPERATION_ERROR);
	}

	@Override
	public boolean update(TaskParcel task) {
		for(TaskParcel t : allTasks) {
			if(t.getId() == task.getId()) {
				t.setTitle(task.getTitle());
				t.setDescription(task.getDescription());
			}
		}
		return false;
	}
}
