package org.todolist.client.model.http;

import java.util.function.Function;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;

public class HttpReqBuilder implements RequestBuilder {
	private BasicAuth auth;
	private Function<Invocation.Builder, Response> action;
	private Function<Req, Req> reqDecoration;
	private UriBuilder uriBuilder;

	public HttpReqBuilder(String base) {
		this.uriBuilder = UriBuilder.fromPath(base);
	}

	@Override
	public RequestBuilder setAuth(BasicAuth auth) {
		this.auth = auth;
		return this;
	}

	@Override
	public RequestBuilder appendPath(String path) {
		uriBuilder = uriBuilder.path(path);
		return this;
	}

	@Override
	public RequestBuilder setAction(Function<Invocation.Builder, Response> action) {
		this.action = action;
		return this;
	}

	@Override
	public RequestBuilder setDecoration(Function<Req, Req> decoration) {
		this.reqDecoration = decoration;
		return this;
	}

	@Override
	public Req getFrom(Client client) {
		String finalPath = uriBuilder.build().toString();
		Invocation.Builder b = client
				.target(finalPath)
				.request();
		if (auth != null)
				b.header(BasicAuth.AUTH, auth.toString());
		if (action == null)
			throw new IllegalStateException("no action has been specified in HttpRequestBuilder");
		Req result = new HttpReq(b, action);
		if (reqDecoration != null)
			result = reqDecoration.apply(result);
		return result;
	}
}