package org.todolist.client.view;

import java.io.IOException;

import org.todolist.client.controll.AppController;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;

public class AppView extends HBox{
	private AppController ctrl;

	@FXML
	private ListView<TaskView> list;

	public AppView(AppController ctrl) {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("AppView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        list.setItems(FXCollections.observableArrayList());
		this.ctrl = ctrl;
	}

	public ObservableList<TaskView> getList(){
		return list.getItems();
	}

	@FXML
	private void add() {
		ctrl.addNewTask();
	}

	@FXML
	private void sync() {
		ctrl.sync();
	}
}
