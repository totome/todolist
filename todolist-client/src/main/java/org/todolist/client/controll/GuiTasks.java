package org.todolist.client.controll;

import org.todolist.client.view.TaskView;
import org.todolist.domain.http.TaskParcel;

import javafx.collections.ObservableList;

public class GuiTasks {
	private ObservableList<TaskView> list;

	public GuiTasks(ObservableList<TaskView> list) {
		this.list = list;
	}

	public boolean add(TaskParcel task, AppController ctrl) {
		return list.add(new TaskView(task, ctrl));
	}

	public void clear() {
		list.clear();
	}
}
