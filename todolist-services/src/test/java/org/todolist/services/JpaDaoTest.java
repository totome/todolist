package org.todolist.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

import java.util.function.Function;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.todolist.services.exception.OperationFailed;

public class JpaDaoTest {
	JpaDao sut; 
	
	@Mock
	EntityManagerFactory emf;
	@Mock
	EntityManager em;
	@Mock
	EntityTransaction et;
	@Mock
	Function<EntityManager, DummyType> action;
	
	class DummyType{
	}
	
	DummyType expected;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this); 
		setDefaultMocksBehaviour();
		sut = new JpaDao(emf);
	}
	
	private void setDefaultMocksBehaviour() {
		Mockito.when(emf.createEntityManager()).thenReturn(em);
		Mockito.when(em.getTransaction()).thenReturn(et);
		Mockito.doNothing().when(et).begin();
		Mockito.doNothing().when(et).commit();
		expected = new DummyType();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void completeTransactionBeginsAndCommitsTransactionWhenItEndsWithoutFailures() {
		Mockito.when(action.apply(em)).thenReturn(expected);
		DummyType actual = sut.completeTransaction(action);
		Mockito.verify(et, Mockito.times(1)).begin();
		Mockito.verify(et, Mockito.times(1)).commit();
		assertEquals(expected, actual);
	}
	
	@Test
	public void whenExceptionOcuresDuringTheTransactionThenTransactionIsNotCommitedAndRollbackIsPerformed() {
		Mockito.when(action.apply(em)).thenThrow(RuntimeException.class);
		
		try{
			sut.completeTransaction(action);
		}catch(RuntimeException ex) {}
		
		Mockito.verify(et, Mockito.times(1)).begin();
		Mockito.verify(et, Mockito.times(1)).rollback();
		Mockito.verify(et, Mockito.never()).commit();
	}
	
	@Test(expected=OperationFailed.class)
	public void whenRuntimeExceptionOcuresDuringTheTransactionThenDaoThrowsOperationFailed() {
		Mockito.when(action.apply(em)).thenThrow(RuntimeException.class);
		sut.completeTransaction(action);
	}

	@Test
	public void completeTransactionReturnsExcaclyTheSameOjectAsReturnedFromAppliedFunction() {
		Mockito.when(action.apply(em)).thenReturn(expected);
		DummyType actual = sut.completeTransaction(action);
		assertSame(expected, actual);
	}

}
