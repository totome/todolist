package org.todolist.client.view;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ErrorMsgView extends GridPane {
	private Stage stg;
	@FXML private Label msg;

	public ErrorMsgView() {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("ErrorMsgView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
	}

	@FXML
	private void close() {
		stg.close();
	}

	public void showError(String cause) {
		msg.textProperty().set(cause);
		stg = new Stage();
		Scene scene = new Scene(this, 250, 100);
		stg.initModality(Modality.APPLICATION_MODAL);
		stg.setTitle("Error");
		stg.setScene(scene);
		stg.show();
	}
}
