package org.todolist.webapp.resource;

import static org.junit.Assert.*;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


import org.mockito.MockitoAnnotations;
import org.todolist.domain.http.UserParcel;
import org.todolist.domain.persistence.User;
import org.todolist.services.TaskDao;
import org.todolist.services.UserDao;

public class UserResourceTest extends JerseyTest{
	private static final String USER_NAME = "12345";
	private static final String PASSWORD = "43438";

	@Mock
	private UserDao mockedDao;
	@Mock
	private TaskDao mockedTaskDao;
	@Mock
	private TaskListResource taskResourceMock;
	@Captor
	private ArgumentCaptor<User> userCaptor;

    @Override
    protected Application configure() {
	MockitoAnnotations.initMocks(this);
	AbstractBinder testBinder = new AbstractBinder() {
		@Override
		protected void configure() {
			bind(mockedDao).to(UserDao.class);
			bind(mockedTaskDao).to(TaskDao.class);
			bind(taskResourceMock).to(TaskListResource.class);
		}
	};

        ResourceConfig testConfig = new ResourceConfig(UserResource.class);
        testConfig.register(testBinder);
        return testConfig;
    }

	@Test
	public void newUserCreation() {
		final User payload = new User(USER_NAME, PASSWORD);
		final Entity<User> userEntity = Entity.entity(payload, MediaType.APPLICATION_JSON);

		when(mockedDao.create(Mockito.any(User.class))).thenReturn(USER_NAME);
		WebTarget tgt = target("/users");
		Response resp = tgt.request().post(userEntity);

		verify(mockedDao).create(userCaptor.capture());
		final User givenUser = userCaptor.getValue();

		assertEquals(USER_NAME, givenUser.getName());
		assertEquals(PASSWORD, givenUser.getPassword());

		assertEquals(201, resp.getStatus());
	}

	@Test
	public void newUserCreationReturnsConflictInResponseWhenUserAlreadyExists() {
		final User payload = new User(USER_NAME, PASSWORD);
		final Entity<User> userEntity = Entity.entity(payload, MediaType.APPLICATION_JSON);
		//this means that user already exists:
		//TODO : consider to redesign this behavior(throw an exception instead?)
		when(mockedDao.create(Mockito.any(User.class))).thenReturn(null);

		Response resp = target("/users").request().post(userEntity);

		assertEquals(409, resp.getStatus());
	}


	@Test
	public void userIsReturnedFromResource() {
		final User payload = new User(USER_NAME, PASSWORD);
		when(mockedDao.read(Mockito.anyString())).thenReturn(payload);

		Response resp = target("/users").path(USER_NAME).request().get();

		verify(mockedDao).read(USER_NAME);

		assertEquals(202, resp.getStatus());
		UserParcel parcel = resp.readEntity(UserParcel.class);
		assertEquals(USER_NAME, parcel.getName());
	}

	@Test
	public void noUserIsReturnedFromResourceIfUserWithGivenNameDoesNotExist() {
		when(mockedDao.read(Mockito.anyString())).thenReturn(null);

		Response resp = target("/users").path(USER_NAME).request().get();

		verify(mockedDao).read(USER_NAME);

		assertEquals(404, resp.getStatus());
		assertNull(resp.readEntity(UserParcel.class));
	}
	
	@Test
	public void userResourceSetsUserNameFromPathToTasksDao(){
		Response resp = target("/users").path(USER_NAME).path("tasks").request().get();
		verify(mockedTaskDao, Mockito.times(1)).setUserName(USER_NAME);
		verify(taskResourceMock).getAll();
	}
}
