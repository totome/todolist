package org.todolist.client.view;

import java.io.IOException;

import org.todolist.client.controll.AppController;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class LoginView extends GridPane{
	private final AppController ctrl;
	@FXML private Label login_warning;
	@FXML private TextField login_field;
	@FXML private TextField password_field;
	@FXML private TextField app_url;
	

	public LoginView(AppController ctrl) {
		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("LoginView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

		this.ctrl = ctrl;
	}
	
	public void setWarning(boolean isVisible) {
		login_warning.setVisible(isVisible);
	}

	@FXML
	private void login() {
		ctrl.newLoginDetails(
				app_url.getText(), 
				login_field.getText(), 
				password_field.getText()
				);
	}
}
