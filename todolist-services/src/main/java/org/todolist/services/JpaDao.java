package org.todolist.services;

import java.util.function.Function;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.todolist.services.exception.OperationFailed;

public class JpaDao {
	private EntityManagerFactory entityManagerFactory;
	
	JpaDao(EntityManagerFactory emf){
		this.entityManagerFactory = emf;
	}
	
	/*
	 * This method is responsible for wrapping EntityManager's transaction by some common code
	 * independent from which JspDao sub-type invokes it.
	 * @param tr should be a function which performs transaction, 
	 * extracts data from obtained result and returns this data.
	 * @return result returned by given function.
	 */
	protected <R> R completeTransaction(Function<EntityManager, ? extends R> tr, OperationFailed failureInfo) {
		EntityManager em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		R result = null;
		try {
			result= tr.apply(em);
			em.getTransaction().commit();
		}catch(OperationFailed myEx) {
			em.getTransaction().rollback();
			throw myEx;
		}catch(RuntimeException trEx) {
			em.getTransaction().rollback();
			failureInfo.initCause(trEx);
			throw failureInfo;
		}finally {
			em.close();
		}
		return result;
	}
	
	protected <R> R completeTransaction(Function<EntityManager, ? extends R> tr) {
		return completeTransaction(tr, new OperationFailed("Persistence operation failed in jpa Dao"));
	}
}
