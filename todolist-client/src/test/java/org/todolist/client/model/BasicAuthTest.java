package org.todolist.client.model;

import static org.junit.Assert.*;

import java.util.Base64;

import org.junit.Test;
import org.todolist.client.model.http.BasicAuth;


public class BasicAuthTest {
	private static String LOGIN = "4*iftt(";
	private static String PASSWORD = "*(&*&*aaa";
	private static String AUTHORIZATION = "Basic " + Base64.getEncoder().encodeToString((LOGIN+":"+PASSWORD).getBytes());
	
	@Test
	public void authObjectReturnsUserNameGivenInConstructor() {
		BasicAuth sut = new BasicAuth(LOGIN, PASSWORD);
		assertEquals(LOGIN, sut.getUserName());
	}
	
	@Test
	public void authObjectReturnsProperlyConstructedAutorizationString() {
		BasicAuth sut = new BasicAuth(LOGIN, PASSWORD);
		assertEquals(AUTHORIZATION, sut.toString());
	}
}
