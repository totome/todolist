package org.todolist.client.view;

import java.io.IOException;

import org.todolist.client.controll.AppController;
import org.todolist.domain.http.TaskParcel;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class TaskView extends VBox {
	private final AppController ctrl;
	private final TaskParcel task;

	@FXML private Label title;
	@FXML private Label desc;
	@FXML private HBox buttons;

	public TaskView(TaskParcel task, AppController ctrl){
		this.task = task;
		this.ctrl = ctrl;

		FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("TaskView.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
	}

	@FXML
	protected void initialize() {
		title.textProperty().set(task.getTitle());
		desc.textProperty().set(task.getDescription());
	}

    @FXML
	private void delete() {
		ctrl.delete(task);
	}

    @FXML
	private void edit() {
		ctrl.editTask(task);
	}
}
