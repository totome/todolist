package org.todolist.webapp.utils;

import java.security.Principal;
import java.util.List;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class CtxUtil{
	private final ContainerRequestContext rctx;
	private final SecurityContext sctx;

	public CtxUtil(ContainerRequestContext requestContext) {
		rctx=requestContext;
		sctx = rctx.getSecurityContext();
	}

	public List<String> getPathParams(String param){
		return rctx.getUriInfo().getPathParameters().get(param);
	}

	public boolean isUserInRole(String role) {
		return sctx.isUserInRole(role);
	}

	public String getUserName() {
		Principal p = rctx.getSecurityContext().getUserPrincipal();

		if (p == null)return null;
		else return p.getName();
	}

	public void abord(String abortCause) {
		rctx.abortWith(Response
				.status(Response.Status.UNAUTHORIZED)
				.tag(abortCause)
				.build());
	}
}