package org.todolist.services.exception;

/*
 * This is common exception.
 * It should be thrown when there is no other suitable choice
 * Every specific exception implemented inside todolist-services should extends this type
 * @param message is the message of this exception
 * TODO : this exception should probably results in internal server error rest message
 */
public class OperationFailed extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6114081638063421399L;

	public OperationFailed(String message) {
		super(message);
	}
}
