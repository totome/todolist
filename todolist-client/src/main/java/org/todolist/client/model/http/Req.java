package org.todolist.client.model.http;

public interface Req {
	Req make();
	<T> T get(Class<T> answerClass);
	int getStatus();
}