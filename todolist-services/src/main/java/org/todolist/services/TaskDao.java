package org.todolist.services;

import java.util.List;

import org.todolist.domain.persistence.Task;

public interface TaskDao {
	void setUserName(String name);
	Long create(Task task);
	Task read(long id);
	Task update(Task task);
	Task delete(long id);
	List<Task> getAll();
}
