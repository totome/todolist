package org.todolist.domain;

import static org.junit.Assert.*;

import java.util.Set;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.ConstraintViolation;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.todolist.domain.persistence.Task;

public class TaskTest {
    private static Validator validator;
    Set<ConstraintViolation<Task>> constraintViolations;
    Task objUnderTest;
    static final Long ID = 0xBAD_F00DL;
    static final Long ID_B = 123L;
    static final String TITLE = "Dummy Title : 8hf787yfhdtfy473h";
    static final String DESC = "Dummy description : nfjrngver8tutb287e";
    static final String TITLE_B = "Dummy Title : 3244kdj";
    static final String DESC_B = "Dummy description : aaa287e";


    @BeforeClass
    public static void setUpValidator() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }
	
    @Before
    public void setUp() {
    	objUnderTest = new Task();
    }
    
	private void setCorrectValuesInObjectUnderTest() {
		objUnderTest = new Task();
		objUnderTest.setId(ID);
		objUnderTest.setTitle(TITLE);
		objUnderTest.setDescription(DESC);
		ensureThatTaskPassesValidation();
	}
	
	private void ensureThatTaskPassesValidation() {
		Set<ConstraintViolation<Task>> 
		constraintViolations = validator.validate(objUnderTest);
		assertEquals("object is not constructed correctly", 0, constraintViolations.size());
	}
	
	private void taskIsEmpty(Task task) {
		assertNull(task.getId());
		assertNull(task.getTitle());
		assertNull(task.getDescription());
	}
	
	private void taskIsInvalid(Task objUnderTest) {
		constraintViolations = validator.validate(objUnderTest);
		assertNotEquals(0, constraintViolations.size());
	}
    
	@Test
	public void idGetterAndSetterWorksProperly() {
		taskIsEmpty(objUnderTest);	
		objUnderTest.setId(ID);
		assertEquals(ID, objUnderTest.getId());
	}
	
	@Test
	public void titleGetterAndSetterWorksProperly() {
		taskIsEmpty(objUnderTest);
		objUnderTest.setTitle(TITLE);
		assertEquals(TITLE, objUnderTest.getTitle());
	}
	
	@Test
	public void descGetterAndSetterWorksProperly() {
		taskIsEmpty(objUnderTest);
		objUnderTest.setDescription(DESC);
		assertEquals(DESC, objUnderTest.getDescription());
	}

	@Test
	public void taskWithoutTitleIsNotValid() {
		setCorrectValuesInObjectUnderTest();
		objUnderTest.setTitle(null);
		taskIsInvalid(objUnderTest);
	}

	@Test
	public void constructorWithAllArgsWorksProperly() {
		Task newTask = new Task(ID, TITLE, DESC);

		assertEquals(ID, newTask.getId());
		assertEquals(TITLE, newTask.getTitle());
		assertEquals(DESC, newTask.getDescription());
	}

	@Test
	public void tasksWithEqualIdsAreEqual() {
		Task first = new Task(ID, TITLE, DESC);
		Task second = new Task(ID, TITLE_B, DESC_B);
		assertTrue(first.equals(second));
		assertTrue(second.equals(first));
	}

	@Test
	public void tasksWithDifferentIdsAreNotEqual() {
		Task first = new Task(ID, TITLE, DESC);
		Task second = new Task(ID_B, TITLE, DESC);
		assertFalse(first.equals(second));
		assertFalse(second.equals(first));
	}

	@Test
	public void emptyTasksAreNotEqual() {
		Task first = new Task();
		Task second = new Task();
		assertFalse(first.equals(second));
		assertFalse(second.equals(first));
	}
}
