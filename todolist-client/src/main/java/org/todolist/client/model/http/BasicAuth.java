package org.todolist.client.model.http;

import java.util.Base64;

public class BasicAuth {
	public static String AUTH = "authorization";
	private final String userName;
	private final String authString;

	public BasicAuth(String userName, String password) {
		this.userName = userName;
		this.authString = "Basic "+ Base64.getEncoder().encodeToString((userName+":"+password).getBytes());
	}

	public String getUserName() {
		return userName;
	}

	public String toString() {
		return authString;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = prime + ((authString == null) ? 0 : authString.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BasicAuth other = (BasicAuth) obj;
		if (authString == null) {
			if (other.authString != null)
				return false;
		} else if (!authString.equals(other.authString))
			return false;
		return true;
	}
}
