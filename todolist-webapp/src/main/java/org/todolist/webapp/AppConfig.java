package org.todolist.webapp;

import org.glassfish.jersey.server.ResourceConfig;
import org.todolist.webapp.filter.UserAuth;

public class AppConfig extends ResourceConfig {

	public AppConfig(){
		register(new ServiceInjectionBinder());
		register(UserAuth.class);
		packages("org.todolist.webapp.resource");
	}
}
