package org.todolist.domain.persistence;

import static org.junit.Assert.*;

import java.lang.Thread.State;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.hibernate.Session;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/**
 * This test tries to keep an eye on an entities {@code equals()}
 * and {@code hashCode()} methods consistency among different
 * persistence states.
 */
public class EQAndHCConsistencyTest {
	static private EntityManagerFactory emf;
	private EntityManager em;
	private EntityTransaction tr;

	@Before
	public void setUp() throws Exception {
		emf = Persistence.createEntityManagerFactory("org.todolist.test");
	}

	@After
	public void tearDown() throws Exception {
		if (em != null && em.isOpen())
			em.close();
		emf.close();
	}

	@Test
	public void RoleInstanceIsRecognizableAsItselfInEveryPersistanceState() {
		Role r = new Role("name");
		assertEqAndHcOfEntityAreConsistent(Role.class, r, e->e.getName());
	}

	@Test
	public void UserInstanceIsRecognizableAsItselfInEveryPersistanceState() {
		User u = new User("name", "password");
		assertEqAndHcOfEntityAreConsistent(User.class, u, e->e.getName());
	}

	@Test
	public void TaskInstanceIsRecognizableAsItselfInEveryPersistanceState() {
		Task t = new Task(null, "some title", "some description");
		assertEqAndHcOfEntityAreConsistent(Task.class, t, e->e.getId());
	}

	private <E, ID> void assertEqAndHcOfEntityAreConsistent(Class<E> clazz, E testedEntity, Function<E, ID> idProvider) {
		Set<E> setOfEntities = new HashSet<>();
		assertFalse(setOfEntities.contains(testedEntity));
		setOfEntities.add(testedEntity);

		assertTrue("An entity in the transient state should be recognized as itself", setOfEntities.contains(testedEntity));

		transaction(em->{
			em.persist(testedEntity);
			em.flush();
		assertTrue("An entity in the managed state should be recognized as itself", setOfEntities.contains(testedEntity));
		});

		assertTrue("An entity in the detached state should be recognized as itself", setOfEntities.contains(testedEntity));

		transaction(em->{
			E entityInOthreCtx = em.merge(testedEntity);
		assertTrue("An reattached entity should be recognized as itself after merge.",
				setOfEntities.contains(entityInOthreCtx));
		});

		transaction(em->{
			Session session = em.unwrap(Session.class);
			session.update(testedEntity);
			assertTrue("An reattached entity should be recognized as itself after update.",
					setOfEntities.contains(testedEntity));
		});

		transaction(em->{
			E entityInOthreCtx = em.find(clazz, idProvider.apply(testedEntity));
			em.flush();
		assertTrue("An entity in the managed state should be recognized as itself after it is fetched by the manager in another ctx",
				setOfEntities.contains(entityInOthreCtx));
		});

		execInOtherThread(this::transaction, em->{
			E entityInOthreCtx = em.find(clazz, idProvider.apply(testedEntity));
			em.flush();
		assertTrue("An entity in the managed state should be recognized as itself after it is fetched by the manager"
					+" in another ctx and another thread",
					setOfEntities.contains(entityInOthreCtx));
		});
	}

	private void execInOtherThread(Consumer<Consumer<EntityManager>> tr, Consumer<EntityManager> f) {
		Thread th = new Thread(()->tr.accept(f));
		th.start();
		try {
			th.join(1000);
		} catch (InterruptedException e) {fail("Something went wrong during the transaction execution");}
		assertEquals("Transaction could not be finished in the given period of time.", State.TERMINATED, th.getState());
	}

	private void transaction(Consumer<EntityManager> f) {
		em = emf.createEntityManager();
		tr = em.getTransaction();
		tr.begin();
		f.accept(em);
		tr.commit();
		em.close();
	}
}
