package org.todolist.webapp.resource;

import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.todolist.domain.http.Link;
import org.todolist.domain.http.UserParcel;
import org.todolist.domain.persistence.User;
import org.todolist.services.TaskDao;
import org.todolist.services.UserDao;

@Path("/users")
public class UserResource {
	@Inject UserDao dao;
	@Inject TaskDao taskDao;

	@Path("{userId}/tasks")
	public Class<TaskListResource> getTasks(@PathParam("userId") String name) {
		taskDao.setUserName(name);
		return TaskListResource.class;
	}

	@Path("{userId}")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@PathParam("userId") String name,
							@Context UriInfo info) {
		User user = dao.read(name);
		if(user == null)
			return Response
					.status(Status.NOT_FOUND)
					.build();

		UserParcel payload = new UserParcel();
		payload.setName(user.getName());
		Set<Link> userLinks = new HashSet<Link>();
		userLinks.add(new Link(info.getRequestUri().toString(),"self"));
		userLinks.add(new Link(info.getRequestUriBuilder().path("tasks").toString(), "tasks"));
		payload.setLinks(userLinks);
		return Response
				.status(Status.ACCEPTED)
				.entity(payload)
				.build();
	}

	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_XML})
	@Produces(MediaType.TEXT_PLAIN)
	public Response setUser(User payload) {
		String name = dao.create(payload);
		if(name == null)
			return Response
					.status(Status.CONFLICT)
					.entity("User with given name already exists. Please choose diffrent user name.").build();
		return Response
				.status(Status.CREATED)
				.entity("new user : "+name)
				.build();
	}
}
