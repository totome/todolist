<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>TODOLIST</title>
	<%@ include file="/WEB-INF/fragments/bootstrapInit.jspf"%>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>
<body style="background-color:lightgray;">
	<%@ include file="/WEB-INF/fragments/navbar.jspf"%>
	<%@ include file="/WEB-INF/fragments/signUpForm.jspf"%>
</body>
</html>