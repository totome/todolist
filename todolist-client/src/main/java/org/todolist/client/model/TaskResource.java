package org.todolist.client.model;

import java.util.List;

import org.todolist.client.model.http.BasicAuth;
import org.todolist.client.model.http.ResourceUnavailable;
import org.todolist.domain.http.TaskParcel;

public interface TaskResource {
	List<TaskParcel> fetchAll();
	boolean create(TaskParcel task);
	boolean delete(TaskParcel task);
	boolean update(TaskParcel task);
	void setConnectionDetails(String path, BasicAuth auth) throws ResourceUnavailable;
}
