package org.todolist.client.view;

import org.todolist.client.controll.AppController;
import org.todolist.client.controll.GuiTasks;
import org.todolist.domain.http.TaskParcel;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class GUI {
	private static final int WIDTH = 800;
	private static final int HEIGHT = 400;
	private static final String BASE_NAME = "todolist";

	private static final class ViewData<T extends Parent> {
		private final T root;
		private final Scene scene;

		ViewData(T root, int width, int height){
			this.root = root;
			this.scene = new Scene(root, width, height);
		}
	}

	private final Stage window;
	private final AppController ctrl;
	private final ViewData<AppView> main;
	private final ViewData<LoginView> login;

	public static void connectGui(Stage st, AppController ctrl) {
		GUI result = new GUI(st, ctrl);
		ctrl.setView(result);
		boolean isWarningVisibile = false;
		result.showLogin(isWarningVisibile);
	}

	private GUI(Stage st, AppController ctrl) {
		this.window = st;
		this.ctrl = ctrl;
		this.main = wrap(new AppView(ctrl));
		this.login = wrap(new LoginView(ctrl));
	}
	
	public void showLogin(boolean isWarningVisibile) {
		login.root.setWarning(isWarningVisibile);
		setScene(login, "login");
	}

	public void showError(String msg) {
		new ErrorMsgView().showError(msg);
	}

	public void showTaskEdition(TaskParcel t) {
		ViewData<?> td = wrap(new TaskDetails(t, ctrl));
		if (t == null)setScene(td, "create task");
		else setScene(td, "Editing : "+t.getTitle());
	}

	public void showMain() {
		setScene(main, "List of tasks");
	}

	public GuiTasks shareTasks() {
		return new GuiTasks(main.root.getList());
	}

	private void setScene(ViewData<?> wrapper, String title) {
		window.setTitle(BASE_NAME+" "+title);
		window.setScene(wrapper.scene);
	}

	private static <T extends Parent> ViewData<T> wrap(T root){
		return new ViewData<T>(root, WIDTH, HEIGHT);
	}
}
