package org.todolist.services;

import static org.junit.Assert.*;

import java.util.function.Consumer;
import java.util.function.Function;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.todolist.domain.persistence.Role;
import org.todolist.domain.persistence.User;

public class UserJpaDaoTest {
	private EntityManagerFactory emf;
	private EntityManager em;
	private EntityTransaction tr;
	private UserJpaDao sut;

	//User and Role dummy constants:
	static final String USER_NAME = "default name";
	static final String WRONG_ID = "badId";
	static final String PASSWORD = "default password";
	static final String USER_ROLE = "user";

	@Before
	public void setUp() throws Exception {
		emf = Persistence.createEntityManagerFactory("org.todolist.DAO.test");
		sut = new UserJpaDao(emf);
		if(transaction(em->{return em.find(Role.class, Role.REGISTERED_USER);}) == null) {
			transaction(em->{
				em.persist(new Role(Role.REGISTERED_USER));
			});
		}
	}

	@After
	public void tearDown() throws Exception {
		if(em != null && em.isOpen())
			em.close();
		emf.close();
	}

	@Test
	public void daoReturnsTheUserIfHeExistsOtherwiseItReturnsNull() {
		assertNumberOfUsers(0);

		String id = sut.create(new User(USER_NAME, PASSWORD));
		User userWhithCorrectId = sut.read(id);
		assertEquals(USER_NAME, userWhithCorrectId.getName());

		assertNotEquals(WRONG_ID, id);

		User userWithWrongId = sut.read(WRONG_ID);
		assertNull(userWithWrongId);
	}

	@Test
	public void daoCreatesNewUserAndThenReadsIt() {
		assertNumberOfUsers(0);

		String id = sut.create(new User(USER_NAME, PASSWORD));

		assertNumberOfUsers(1);
		assertEquals(USER_NAME, id);

		User user = sut.read(USER_NAME);

		assertEquals(USER_NAME, user.getName());
		assertEquals(PASSWORD, user.getPassword());
		assertTrue(user.isInRole(Role.REGISTERED_USER));
	}

	@Test
	public void daoDoesNothingWhenAskedToCreateUserWhoAlreadyExists() {
		assertNumberOfUsers(0);
		sut.create(new User(USER_NAME, PASSWORD));
		assertNumberOfUsers(1);
		String id = sut.create(new User(USER_NAME, PASSWORD));
		assertNumberOfUsers(1);
		assertNull(id);
	}

	private void  assertNumberOfUsers(long num) {
		long initialNumberOfUsers = transaction(em->{return em.createQuery(
				"SELECT COUNT(e) FROM User e",
				Long.class).getSingleResult();});
		assertEquals(num, initialNumberOfUsers);
	}

	private <T> T transaction(Function<EntityManager, T> f) {
		em = emf.createEntityManager();
		tr = em.getTransaction();
		tr.begin();
		T result = f.apply(em);
		tr.commit();
		em.close();
		return result;
	}

	private void transaction(Consumer<EntityManager> f) {
		em = emf.createEntityManager();
		tr = em.getTransaction();
		tr.begin();
		f.accept(em);
		tr.commit();
		em.close();
	}
}
