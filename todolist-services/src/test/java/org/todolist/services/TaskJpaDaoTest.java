package org.todolist.services;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.todolist.domain.persistence.Task;
import org.todolist.domain.persistence.User;
import org.todolist.services.exception.DataNotFound;

public class TaskJpaDaoTest {
	static final String USER_NAME = "default name";
	static final String PASSWORD = "default password";
	static final User USER = new User(USER_NAME, PASSWORD);
	
	static final String ANOTHER_USER_NAME = "name 123456";
	static final User ANOTHER_USER = new User(ANOTHER_USER_NAME, PASSWORD);
	
	private static final long DUMMY_ID = 0xC00FEE;
	private Task DUMMY_TASK;
	private Task DUMMY_TASK_B;
	private Task DUMMY_TASK_C;
	
	private EntityManagerFactory emf;
	private EntityManager em;
	private EntityTransaction tr;
	
	TaskJpaDao objUnderTest;
	
	@Before
	public void setUp() throws Exception {
		DUMMY_TASK = new Task(null, "title3287", "desc7843");
		DUMMY_TASK_B = new Task(null, "0324title", "3204desc");
		DUMMY_TASK_C = new Task(null, "This is the title", "This is some description.");
		
		emf = Persistence.createEntityManagerFactory("org.todolist.DAO.test");
		
		objUnderTest = new TaskJpaDao(emf);
		transaction(em->{
			em.persist(USER);
			em.persist(ANOTHER_USER);	
		});
		objUnderTest.setUserName(USER_NAME);
	}

	@After
	public void tearDown() throws Exception {
		if(em != null && em.isOpen())
			em.close();
		emf.close();
	}
	
	@Test
	public void whenCreateIsInvokedThenDaoPersistsGivenTask() {
		assertNumberOfTasks(0);
		final Long id = objUnderTest.create(DUMMY_TASK);
		assertIsPresent(DUMMY_TASK);
	}

	@Test
	public void readingMethodReturnsProperlyObtainedObject() {
		saveAsDefaultUsersTask(DUMMY_TASK);
		Task result = objUnderTest.read(DUMMY_TASK.getId());
		assertTaskProperitiesEquals(DUMMY_TASK, result);
	}
	
	@Test
	public void getAllTasksMethodReturnsProperlyObtainedObjects() {
		saveAsDefaultUsersTask(DUMMY_TASK);
		saveAsDefaultUsersTask(DUMMY_TASK_B);
		saveAsAnotherUsersTask(DUMMY_TASK_C);
		assertNumberOfTasks(3);
		
		List<Task> resultList = objUnderTest.getAll();
		
		assertEquals(2, resultList.size());
		assertTrue(resultList.contains(DUMMY_TASK));
		assertTrue(resultList.contains(DUMMY_TASK));
	}
	
	@Test
	public void getAllTasksMethodReturnsEmptyListWhenUserHasNoTasks() {
		saveAsDefaultUsersTask(DUMMY_TASK);
		saveAsDefaultUsersTask(DUMMY_TASK_B);
		saveAsDefaultUsersTask(DUMMY_TASK_C);
		assertNumberOfTasks(3);
		
		objUnderTest.setUserName(ANOTHER_USER_NAME);
		
		List<Task> resultList = objUnderTest.getAll();
		
		assertEquals(0, resultList.size());
	}
	
	@Test 
	public void updateUpdatesTaskAndReturnsMergedTask() {
		saveAsDefaultUsersTask(DUMMY_TASK);
		assertNumberOfTasks(1);

		//task b has the same id but other properties are different:
		DUMMY_TASK_B.setId(DUMMY_TASK.getId());
		assertTaskProperitiesNotEquals(DUMMY_TASK, DUMMY_TASK_B);

		Task result = objUnderTest.update(DUMMY_TASK_B);

		assertTaskProperitiesEquals(DUMMY_TASK_B, result);
		assertIsPresent(result);
		assertNumberOfTasks(1);
	}
	
	@Test 
	public void deleteDeletesTaskAndReturnsDeletedTask() {
		saveAsDefaultUsersTask(DUMMY_TASK);
		assertIsPresent(DUMMY_TASK);
		final Long ID = DUMMY_TASK.getId();

		objUnderTest.delete(ID);
		assertNumberOfTasks(0);
	}
	
	//throwing DataNotFound:
	
	@Test(expected=DataNotFound.class)
	public void whenDeleteIsInvokedAndWantedTaskIsNotPreasentThenDataNotFoundShouldBeThrown() {
		assertNumberOfTasks(0);
		objUnderTest.delete(DUMMY_ID);
	}
	
	@Test(expected=DataNotFound.class)
	public void whenUpdateIsInvokedAndWantedTaskIsNotPreasentThenDataNotFoundShouldBeThrown() {
		assertNumberOfTasks(0);
		DUMMY_TASK.setId(DUMMY_ID);
		objUnderTest.update(DUMMY_TASK);
	}
	
	@Test(expected=DataNotFound.class)
	public void whenReadIsInvokedAndWantedTaskIsNotPreasentThenDataNotFoundShouldBeThrown() {
		assertNumberOfTasks(0);
		objUnderTest.read(DUMMY_ID);
	}
	
	public void accessToTheOtherUsersTasksMustNotBePermitted() {
		saveAsAnotherUsersTask(DUMMY_TASK);
		assertIsPresent(DUMMY_TASK);
		
		try {
			objUnderTest.read(DUMMY_TASK.getId());	
			fail("access to the other users data must not be permitted");
		} catch(DataNotFound ex) {
		}
		
		try {
			objUnderTest.update(DUMMY_TASK);	
			fail("access to the other users data must not be permitted");
		} catch(DataNotFound ex) {
		}
		
		try {
			objUnderTest.delete(DUMMY_TASK.getId());	
			fail("access to the other users data must not be permitted");
		} catch(DataNotFound ex) {
		}
	}

	private void  assertNumberOfTasks(long num) {
		long initialNumberOfUsers = transaction(em->{return em.createQuery(
				"SELECT COUNT(e) FROM Task e",
				Long.class).getSingleResult();});
		assertEquals(num, initialNumberOfUsers);
	}

	private void assertIsPresent(final Task task) {
		final Task taskFromDB = (Task) transaction(em->{
			User user = em.find(User.class, USER_NAME);
			Set<Task> l = user.getTasks();
			Task t = l.stream()
					.filter(e->e.getId()==task.getId())
					.findFirst()
					.orElseThrow(()->new NoSuchElementException(
							"assertIfPresent : Task with id = "+task.getId()+" not found"));
			return t;
		});
		assertTaskProperitiesEquals(task, taskFromDB);
	}

	private void saveAsDefaultUsersTask(final Task task) {
		transaction(em->{
			em.persist(task);
			em.find(User.class, USER_NAME).getTasks().add(task);
		});
	}
	
	private void assertTaskProperitiesNotEquals(Task a, Task b){
		assertFalse(a.getTitle() == b.getTitle()
				&& a.getDescription() == b.getDescription()
				&& a.getId() == b.getId());
	}

	private void assertTaskProperitiesEquals(Task a, Task b) {
		assertEquals(a.getId(), b.getId());
		assertEquals(a.getTitle(), b.getTitle());
		assertEquals(a.getDescription(), b.getDescription());
	}

	private void saveAsAnotherUsersTask(final Task task) {
		transaction(em->{
			em.persist(task);
			em.find(User.class, ANOTHER_USER_NAME).getTasks().add(task);
		});
	}

	private <T> T transaction(Function<EntityManager, T> f) {
		em = emf.createEntityManager();
		tr = em.getTransaction();
		tr.begin();
		T result = f.apply(em);
		tr.commit();
		em.close();
		return result;
	}

	private void transaction(Consumer<EntityManager> f) {
		em = emf.createEntityManager();
		tr = em.getTransaction();
		tr.begin();
		f.accept(em);
		tr.commit();
		em.close();
	}
}
