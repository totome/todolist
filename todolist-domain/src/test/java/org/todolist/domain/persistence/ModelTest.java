package org.todolist.domain.persistence;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/**
 * The main purpose of this tests is to examine the most crucial points 
 * of the persistence model (relations, annotations, etc.) and check that the tables
 * created by JPA agree with expectations where it is required.
 */
public class ModelTest {
	//Task dummy constants:
	static private final String TITLE = "fdjr893y74";
	static private final String DESC = "edy5873y";
	//User and Role dummy constants:
	static final String USER = "default name";
	static final String USER_B = "another user";
	static final String PASS = "default password";
	static final String ROLE = "dummy access level";
	
	static private EntityManagerFactory emf;
	private EntityManager em;
	private EntityTransaction tr;
	
	@Before
	public void setUp() throws Exception {
		emf = Persistence.createEntityManagerFactory("org.todolist.test");
	}

	@After
	public void tearDown() throws Exception {
		if(em != null && em.isOpen())
			em.close();
		emf.close();
	}

	@Test
	public void TaskIsProperlyPersistedWithNewId() {
		final Task toPersist = new Task(null, TITLE, DESC);
		final User user = new User(USER, PASS);
		
		transaction(em->{
			em.persist(user);
			user.getTasks().add(toPersist);
		});
		
		assertNotNull(toPersist.getId());
		
		final Task fromJPA = 
		transaction(em->{
			Task t = em.find(Task.class, toPersist.getId());
			return t;
		});
		
		assertEquals(TITLE, fromJPA.getTitle());
		assertEquals(DESC, fromJPA.getDescription());
		assertEquals(USER, fromJPA.getUserName());
	}
	
	@Test
	public void WhenTheNewRoleIsAddedByTheUser_thenThisUserIsPresentInThatRole() {
		final User user = new User(USER, PASS);
		final Role role = new Role(ROLE);
		transaction(em->{
			em.persist(role);
			em.persist(user);
			user.addRoles(role);
		});
		
		final Set<User> usersWithDummyRole =
		transaction(em->{
			Role dummyRole = em.find(Role.class, ROLE);
			return dummyRole.getMembers();
		});
		
		assertEquals(1, usersWithDummyRole.size());
		final User newU = usersWithDummyRole.iterator().next();
		assertEquals(USER, newU.getName());
		assertEquals(PASS, newU.getPassword());
	}
	
	@Test
	public void whenTheNewUserIsAddedToTheRole_thenThisUserIsInThisRole() {		
		final User user = new User(USER, PASS);
		final Role role = new Role(ROLE);		
		transaction(em->{
			em.persist(role);
			em.persist(user);
			role.addUsers(user);
		});
		
		final boolean isInRole =
		transaction(em->{
			User userFromDb = em.find(User.class, USER);
			return userFromDb.isInRole(ROLE);
		});
		
		assertEquals(true, isInRole);
	}
	
	@Test
	public void JPACreatesPivotTable_user_role_AndUseItToStoreRelationshipInfo() {
		final User user = new User(USER, PASS);
		final Role role = new Role(ROLE);	
		transaction(em->{
			em.persist(user);
			em.persist(role);
			user.addRoles(role);
		});
		
		@SuppressWarnings("unchecked")
		List<Object[]> result = 
		transaction(em->{
			//name of the table and it's columns must agree with those from Tomcat's Realm element in context.xml
			return em.createNativeQuery("SELECT name, role FROM user_role WHERE name=:userId AND role=:roleId")
			.setParameter("userId", USER)
			.setParameter("roleId", ROLE)
			.getResultList();
		});
		
		assertEquals(1, result.size());
		Object[] tab = result.iterator().next();
		String userName = tab[0].toString();
		String roleName = tab[1].toString();
		assertEquals(USER, userName);
		assertEquals(ROLE, roleName);
	}
	
	@Test
	public void everyUserHasHisOwnTasks() {
		final User user = new User(USER, PASS);
		final User userB = new User(USER_B, PASS);
		final Task task = new Task(null, "Users A task", DESC);
		final Task taskB = new Task(null, "Users B task", DESC);

		transaction(em->{
			em.persist(user);
			em.persist(userB);
			
			user.getTasks().add(task);
			userB.getTasks().add(taskB);
		});
		
		List<Task> tasks = transaction(em->{
			User u = em.find(User.class, USER);
			return new ArrayList<Task>(u.getTasks());
		});
		
		List<Task> tasksB = transaction(em->{
			User u = em.find(User.class, USER_B);
			return u.getTasks().stream().collect(Collectors.toList());
		});
		
		assertTrue(tasks.contains(task));
		assertEquals(1, tasks.size());
		assertTrue(tasksB.contains(taskB));
		assertEquals(1, tasks.size());
	}
	
	private <T> T transaction(Function<EntityManager, T> f) {
		em = emf.createEntityManager();
		tr = em.getTransaction();
		tr.begin();
		T result = f.apply(em);
		tr.commit();
		em.close();
		return result;
	}

	private void transaction(Consumer<EntityManager> f) {
		em = emf.createEntityManager();
		tr = em.getTransaction();
		tr.begin();
		f.accept(em);
		tr.commit();
		em.close();
	}
}
