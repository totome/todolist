package org.todolist.domain.persistence;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;

/**
 * as for now this class exists only for the sake of 
 * compatibility with tomcat's authentication mechanism
 */
@Entity
@Table(name="roles")
public class Role {
	public static final String REGISTERED_USER = "user";
	private String name;
	private Set<User> users;
	
	public Role() {
		this.users = new HashSet<User>();
	}
	
	public Role(String roleName) {
		this();
		this.name = roleName;
	}
		
	/* Annotations of the method below must agree with the
	 * corresponding attributes of the Tomcat's authentication 
	 * settings (attributes of the Realm element) in the file:
	 * todolist-webapp/src/main/webapp/META-INF/context.xml
	 */
	@ManyToMany(
			fetch=FetchType.LAZY)
	@JoinTable(
			name = "user_role",
			joinColumns = {@JoinColumn(name = "role")},
			inverseJoinColumns = {@JoinColumn(name = "name")},
			uniqueConstraints = {@UniqueConstraint(columnNames = {"role", "name"})})
	protected Set<User> getUsers() {
		return users;
	}
	
	protected void setUsers(Set<User> users) {
		this.users = users;
	}
	
	@Id
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void addUsers(User...newUsers) {
		for (User u : newUsers) {
			getUsers().add(u);
			u.getRoles().add(this);
		}
	}
	
	@Transient
	public Set<User> getMembers() {
		return new HashSet<User>(getUsers());
	}

	@Override
	public int hashCode() {
		return ((name == null) ? 0 : name.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) return true;
		if (! (obj instanceof Role)) return false;
		Role other = (Role) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}


}
